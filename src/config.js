//export const BASE_URL = 'http://192.168.31.199:8808'
//export const  BASE_URL = 'http://150.95.104.158'
export const BASE_URL = 'http://quanlycudan.com'


export const URL_API = BASE_URL + '/API'

export function removeUnicode(value) {
  if (typeof value === 'undefined') {
    return ''
  }
  value = value
    .toString()
    .toLowerCase()
  value = value.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, 'a')
  value = value.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, 'e')
  value = value.replace(/ì|í|ị|ỉ|ĩ/g, 'i')
  value = value.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, 'o')
  value = value.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, 'u')
  value = value.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, 'y')
  value = value.replace(/đ/g, 'd')
  return value
}

export function createSlug(str) {
  if (str !== null) {
    str = str.toLowerCase()
    str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, 'a')
    str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, 'e')
    str = str.replace(/ì|í|ị|ỉ|ĩ/g, 'i')
    str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, 'o')
    str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, 'u')
    str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, 'y')
    str = str.replace(/đ/g, 'd')
    str = str.replace(/!|@|%|\^|\ \(|\)|\+|\=|\<|\>|\?|\/|,|\.|\ \;|\'|\"|\&|\#|\[|\]|~|$| |_/g, '-')
    str = str.replace(/-+-/g, '-') // thay thế 2- thành 1-
    str = str.replace(/^\-+|\-+$/g, '')
    return str;
  }
  return '';
}

export function getBearerToken() {
  return 'Bearer ' + localStorage.getItem('token');
}

export function _VN(_number, callback) {
  if (_number == null || typeof _number === 'undefined') _number = '0';
  var resString = '';
  var counter = 0;
  var param = (_number.toString().split('.').join(''))
    .toString()
    .split(',')
    .join('.')
    .split('.');
  var mString = (parseInt(param[0])).toString();
  for (let i = mString.length - 1; i >= 0; i--) {
    resString = mString[i] + resString
    counter += 1;
    if (counter % 3 === 0 && i !== 0 && param[0][i - 1] !== '-') {
      resString = '.' + resString
    };
  }
  callback(resString + (typeof param[1] === 'undefined' ?
    '' :
    ',' + param[1]));
}

export function _ProgramFloat(_input, callback) {
  return _input
    .toString()
    .split('.')
    .join('')
    .split(',')
    .join('.');
}

import jwt from 'json-web-token'

export function PermissionAllow(_group, _level, callback) {
  let resData = false;
  EB.$emit('Verify_TOKEN');
  while (true) {
    if (window.localStorage.getItem('_token') === '' || window.localStorage.getItem('token') === null) {
      break;
    }
    if (window.localStorage.getItem('token') === '' || window.localStorage.getItem('token') === null) {
      break;
    }
    if (window.localStorage.getItem('token_') === '' || window.localStorage.getItem('token_') === null) {
      break;
    }
    if (window.localStorage.getItem('token__') === '' || window.localStorage.getItem('token__') === null) {
      break;
    }
    let dataa = window
      .localStorage
      .getItem('token_');
    let datab = window
      .localStorage
      .getItem('token__');

    jwt.decode(SECRET_KEY, window.localStorage.getItem('_token'), (eee, ddd) => {
      if (!eee) {
        let per_role = [];
        try {
          jwt.decode(SECRET_KEY, dataa, (err, res) => {
            if (!err)
              jwt.decode(SECRET_KEY, datab, (err2, res2) => {
                if (!err2) {
                  let c = res2
                    .data
                    .toString()
                    .split('')
                    .reverse()
                    .join('');
                  if (c === res.data) {
                    if (ddd.data.toString() === 'admin') resData = true;
                    else {
                      if (ddd.data.toString() === 'manager' || ddd.data.toString() === 'staff') {
                        let roleTab = JSON.parse(c);
                        if (parseInt(roleTab[_group]) >= _level) {
                          resData = true;
                        }
                      }
                    }
                  }
                }
              });
          });
        } catch (e) {
          EB.$emit('changeLocalData');
        }
      }
    })



    break;
  }
  if (resData)
    callback(resData);
  else {
    window.localStorage.clear();
    window.location.href = '/dang-nhap';
  }
};

export function _removeAt(arr, index, cb) {
  let res = [];
  for (let i = 0; i < arr.length; i++) {
    if (i !== index) {
      res.push(arr[i]);
    }
  };
  cb(res);
};

import Vue from 'vue';

export const EB = new Vue();
export const API = {
  Login: URL_API + '/user/authenticate',
  group_Create: URL_API + '/group/create',
  group_Get: URL_API + '/group/list',
  group_ChangeState: URL_API + '/group/change',
  group_DeleteGroup: URL_API + '/group/remove?_id=',
  group_Update: '',
  staff_Create: URL_API + '/user/staff/create',
  staff_GetList: URL_API + '/user/staff/list',
  staff_Update: URL_API + '/user/staff/edit',
  staff_Remove: URL_API + '/user/staff/remove?_id=',
  staff_ChangeState: URL_API + '/user/staff/change',
  staff_ResetPassword: URL_API + '/user/staff/reset',
  user_ChangeState: URL_API + '/user/change',
  user_ResetPassword: URL_API + '/user/reset-password',
  user_Delete: URL_API + '/user/remove?_id=',
  user_Update: URL_API + '/user/edit',
  hotLineConfig_Create: URL_API + '/hotline/config/create',
  hotLineConfig_Get: URL_API + '/hotline/config/list',
  hotLineConfig_Update: URL_API + '/hotline/config/edit',
  hotlineConfig_ChangeState: URL_API + '/hotline/config/change',
  hotlineConfig_Delete: URL_API + '/hotline/config/remove?_id=',
  hotline_Create: URL_API + '/hotline/create',
  hotline_Get: URL_API + '/hotline/list',
  hotline_Update: URL_API + '/hotline/edit',
  hotline_Delete: URL_API + '/hotline/remove?_id=',
  hotline_ChangeState: URL_API + '/hotline/change',
  currentUser_changePassword: URL_API + '/user/change-password',
  currentUser_getProfile: URL_API + '/user/profile',
  currentUser_UpdateProfile: URL_API + '/user/update',
  currentUser_History: URL_API + '/his/list',
  group_getRole: URL_API + '/roles/get?groupId=',
  group_roleUpdate: URL_API + '/roles/update',
  building_Create: URL_API + '/building/create',
  building_Get: URL_API + '/building/list',
  building_ChangeState: URL_API + '/building/change',
  building_Delete: URL_API + '/building/remove?_id=',
  resident_Create: URL_API + '/user/resident/create',
  resident_Get: URL_API + '/user/resident/list',
  resident_Update: URL_API + '/user/resident/edit',
  resident_Delete: URL_API + '/user/resident/remove?_id=',
  resident_Import: URL_API + '/user/import',
  resident_ChangeState: URL_API + '/user/resident/change',
  resident_UseApp: URL_API + 'xxxxxxxxx',
  resident_Active: URL_API + '/user/resident/active',
  resident_ResetPassword: URL_API + '/user/resident/reset',
  resident_GetListByRoom: URL_API + '/user/resident/list?room=',
  resident_GetNotActive: URL_API +'/user/resident/not-active',
  notification_Create: URL_API + '/ntf/create',
  notification_Update: URL_API + '/ntf/edit',
  notification_Get: URL_API + '/ntf/list',
  notification_Detail: URL_API + '/ntf/detail?_id=',
  notification_Delete: URL_API + '/ntf/remove?_id=',
  notification_Statistical: URL_API + '/ntf/statistical',
  notification_Send: URL_API + '/ntf/fee/push', //post {_id: [],}
  fileUpload: URL_API + '/util/upload',
  floor_getById: URL_API + '/building/floor?_id=',
  opinionConfig_Create: URL_API + '/opinion/op/create',
  opinionConfig_Get: URL_API + '/opinion/op/list',
  opinionConfig_ChangeState: URL_API + '/opinion/op/change',
  opinion_Create: URL_API + '/opinion/create',
  opinion_Get: URL_API + '/opinion/list',
  opinion_DetailById: URL_API + '/opinion/detail?_id=',
  opinion_Commment: URL_API + '/opinion/comment',
  opinion_Seen: URL_API + '/opinion/seen',
  opinion_Done: URL_API + '/opinion/done',
  opinion_GetNotActive: URL_API +'/opinion/not-seen',
  noteBookConfig_Create: URL_API + '/nb/config/create',
  noteBookConfig_Get: URL_API + '/nb/config/list',
  noteBookConfig_Update: URL_API + '/nb/config/edit',
  noteBookConfig_ChangeState: URL_API + '/nb/config/edit',
  noteBook_Create: URL_API + '/nb/create',
  noteBook_Get: URL_API + '/nb/list',
  noteBook_DetailById: URL_API + '/nb/detail?_id=',
  noteBook_Update: URL_API + '/nb/edit',
  noteBook_ChangeState: URL_API + '/nb/edit',
  noteBook_Delete: URL_API + '/nb/remove?_id=',
  survey_Create: URL_API + '/sur/create',
  survey_Get: URL_API + '/sur/list',
  survey_Update: URL_API + '/sur/edit',
  survey_ChangeState: URL_API + '/sur/change',
  survey_Delete: URL_API + '/sur/remove?_id=',
  survey_Detailt: URL_API + '/sur/detail?_id=',
  survey_GetStatistical: URL_API + '/sur/statistical?_id=',
  serviceFee_Update: URL_API + '/ser/update',
  serviceFee_Get: URL_API + '/ser/list',
  serviceFee_GetListMonth: URL_API + '/ntf/fee/list',
  serviceFee_GetDetailByMonth: URL_API + '/ntf/fee/detail?month=',
  serviceFee_UpdateMonthOfObject: URL_API + '/ntf/fee/edit',
  serviceFee_Import: URL_API +'/fee/import?month=',
  serviceConfig_Create: URL_API + '/fee/create',
  serviceConfig_List: URL_API + '/fee/list',
  serviceConfig_Update: URL_API +'/fee/edit',
  serviceConfig_ChangeState: URL_API +'/opinion/op/change',
  serviceConfig_Remove: URL_API +'/fee/remove?_id=',
  room_Create: URL_API + '/room/create',
  room_GetListByBuilding: URL_API + '/room/list?building=',
  room_Update: URL_API + '/room/edit',
  room_Get: URL_API + '/room/list',
  room_File_Template: URL_API +'/util/download/data/room_template',
  room_File_Data: URL_API+'/util/download/data/room_data',
  room_Import_File: URL_API +'/room/import',
  contact_Send: URL_API + '/contact/send',
  contact_Get: URL_API + '/contact/list',
  contact_Remove: URL_API + '/contact/remove?_id=',
  apartment_Create: URL_API + '/apartment/add',
  apartment_Get: URL_API + '/apartment/list',
  file_Template: URL_API + '/util/download/data/resident_template',
  file_Data: URL_API + '/util/download/data/resident_data',
  file_Template_Fee: URL_API +'/util/download/data/fee_template',
  file_Data_Fee: URL_API +'/util/download/data/fee_data',
};

export function getMenuManager(cb) {
  let resData = 'NULL';
  if (window.localStorage.getItem('token_') !== '' && window.localStorage.getItem('token_') !== null) {
    jwt.decode(SECRET_KEY, window.localStorage.getItem('token_'), (err, res) => {
      if (err);
      else {
        jwt.decode(SECRET_KEY, window.localStorage.getItem('_token'), (eee, ddd) => {
          if (ddd.data.toString() === 'manager' || ddd.data.toString() === 'staff') {
            resData = JSON.parse(res.data);
          } else {
            resData = {
              admin: 99
            };
          }
        });

      }
    });
  };
  cb(resData);
};

export const cf = {
  headers: {
    Authorization: getBearerToken(),
    'Content-Type': 'application/json'
  }
};
export const UploadFolder = BASE_URL + '/assets/upload/';
export const SECRET_KEY = 'HOANGTHIENLOC_M';
